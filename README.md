## Keylogger using RSA encryption

This project is a keylogger application that records each keystroke from the keyboard when the user types in a text file, encrypts them using RSA cryptography,
and stores them in a file which then can be decrypted later.

# Dependencies:

- Python 3
- Pynput 1.7.3
- Pycryptodome 3.10.1

# Pre-requisites
1. Python and pip package manager
2. Knowledge about [RSA algorithm & it's implementation](https://www.geeksforgeeks.org/rsa-algorithm-cryptography/)

# Working

1. Install Python 3 and pip.
2. Run the command
   
     ```git clone https://gitlab.com/vishu-25/iss-keylogger.git```

3. Change to project directory 
   
     ```cd iss-keylogger/``` 

4. To install all dependencies 
   
   ```pip install -r requirements.txt```

5. Generating keys
   
    ```python3 key_generator.py``` 

6. Running the keylogger 
    
   ```python3 keylogger.py```

7. Press `escape` or `ctrl+c` to exit the program
8. For output, the run this command:
   
    ```python3 decryption.py``` 

# Output
   
   ![Output of the program](images/output.png)

# Disclaimer

This project is for educational purposes only. Using a keylogger without consent is illegal and unethical. The creators of this project are not responsible for any damages or illegal activities
that result from the use of this project