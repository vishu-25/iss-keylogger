from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import base64


def encrypt_message(message):
    with open('public.pem', 'rb') as f:
        public_key = RSA.import_key(f.read())
        cipher = PKCS1_OAEP.new(public_key)
        encrypted_message = cipher.encrypt(message.encode())
        return base64.b64encode(encrypted_message).decode()

    
def decrypt_message(encrypted_message):
    with open('private.pem', 'rb') as f:
        private_key = RSA.import_key(f.read())
        encrypted_message = base64.b64decode(encrypted_message.encode())
        cipher = PKCS1_OAEP.new(private_key)
        message = cipher.decrypt(encrypted_message).decode()
        return message
