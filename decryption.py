import encryption


with open('encrypted_message.txt', 'r') as f:
    encrypted_message = f.read()
    decrypted_message = encryption.decrypt_message(encrypted_message)
    print(decrypted_message)
